﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ZaaksysteemUpload
{
    [DataContract]
    public class ConfigurationDataItem : IEquatable<ConfigurationDataItem>
    {
        [DataMember(IsRequired = true)]
        public string Name { get; set; }

        [DataMember(IsRequired = true)]
        public string APIKey { get; set; }

        [DataMember(IsRequired = true)]
        public string PostURL { get; set; }

        [DataMember(IsRequired = true)]
        public string WatchPath { get; set; }

        #region Equality methods

        public bool Equals(ConfigurationDataItem other)
        {
            if (other == null) return false;

            return string.Equals(Name, other.Name)
                && string.Equals(APIKey, other.APIKey)
                && string.Equals(PostURL, other.PostURL)
                && string.Equals(WatchPath, other.WatchPath);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals(obj as ConfigurationDataItem);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 983;
                hash = hash * 2557 ^ (string.IsNullOrEmpty(Name) ? 0 : Name.GetHashCode());
                hash = hash * 2557 ^ (string.IsNullOrEmpty(APIKey) ? 0 : APIKey.GetHashCode());
                hash = hash * 2557 ^ (string.IsNullOrEmpty(PostURL) ? 0 : PostURL.GetHashCode());
                hash = hash * 2557 ^ (string.IsNullOrEmpty(WatchPath) ? 0 : WatchPath.GetHashCode());
                return hash;
            }
        }

        #endregion
    }

    [DataContract]
    public class ConfigurationData
    {
        [DataMember(IsRequired = true)]
        public List<ConfigurationDataItem> ConfigurationItems;

        [DataMember(IsRequired = true)]
        public uint HTTPTimeoutMinutes { get; set; }
    }
}
