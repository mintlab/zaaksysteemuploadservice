﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.ServiceModel;

namespace ZaaksysteemUpload
{
    [ServiceContract]
    public interface IConfigurationService
    {
        [OperationContract]
        ConfigurationData GetConfiguration();

        [OperationContract]
        void SetConfiguration(ConfigurationData config);
    }
}
