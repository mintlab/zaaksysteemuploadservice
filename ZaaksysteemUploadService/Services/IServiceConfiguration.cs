﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZaaksysteemUpload;

namespace ZaaksysteemUploadService
{
    interface IServiceConfiguration
    {
        uint HTTPTimeoutMinutes { get; }
        List<ConfigurationDataItem> GetConfigurationItems();
        void SetConfiguration(ConfigurationData config);
    }
}
