﻿#region Software License
/*
    Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Diagnostics;

namespace ZaaksysteemUploadService
{
    class Logger : ILogger
    {
        public EventLog EventLog;

        public void WriteEntry(string message)
        {
            WriteEntry(message, EventLogEntryType.Information);
        }

        public void WriteEntry(string message, EventLogEntryType type)
        {
            DebugEntry(message);
            EventLog.WriteEntry(message, type);
        }

        public void DebugEntry(string message)
        {
            Debug.WriteLine(message);
        }
    }
}
