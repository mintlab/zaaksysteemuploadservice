﻿#region Software License
/*
    Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Permissions;
using System.Threading;
using System.Timers;

namespace ZaaksysteemUploadService
{
    class WatcherThread {
        Dictionary<string, System.Timers.Timer> Timeouts = new Dictionary<string, System.Timers.Timer>();
        public ILogger Logger;
        internal BlockingCollection<UploadableFile> UploadQueue;
        internal CancellationToken CancellationToken;
        internal ServiceConfiguration ServiceConfiguration;
        internal string Version;

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public void Run()
        {
            Logger.DebugEntry("Started new watcher thread");

            var watchers = new List<FileSystemWatcher>();
            foreach (var config in ServiceConfiguration.GetConfigurationItems())
            {
                FileSystemWatcher watcher = new FileSystemWatcher();
                watcher.Path = config.WatchPath;
                watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

                var u = new UploadDestination
                {
                    Name = config.Name,
                    APIKey = config.APIKey,
                    PostURL = config.PostURL
                };

                // Add event handlers.
                watcher.Created += (src, e) => OnFileCreated(src, e, u);
                watcher.Changed += (src, e) => OnFileChanged(src, e, u);

                // This starts the actual watching of the watch folder.
                watcher.EnableRaisingEvents = true;

                Logger.WriteEntry("Watching " + watcher.Path + " for new files.");
                watchers.Add(watcher);
            }

            if (watchers.Count == 0)
            {
                Logger.WriteEntry("No paths configured. Not doing anything. Run the configuration tool.", EventLogEntryType.Warning);
                return;
            }

            // Wait for the shutdown signal to be raised (check every second).
            while (!CancellationToken.IsCancellationRequested)
            {
                Thread.Sleep(1000);
            }
            Logger.DebugEntry("Cancellation requested.");

            foreach (var w in watchers)
            {
                Logger.DebugEntry("Stopping watcher for " + w.Path);
                w.Dispose();
            }

            return;
        }

        private void OnFileCreated(object source, FileSystemEventArgs e, UploadDestination u)
        {
            // Set a timer (for 10 seconds?)
            Logger.WriteEntry("Found a new file: " + e.FullPath);
            
            StartTimerForPath(e.FullPath, u);
        }

        private void OnFileChanged(object source, FileSystemEventArgs e, UploadDestination u)
        {
            // If the file gets deleted, remove timeout and stop watching.
            if (e.ChangeType == WatcherChangeTypes.Deleted)
            {
                RemoveTimerForPath(e.FullPath);
                return;
            }

            RestartTimerForPath(e.FullPath, u);
        }

        private void RestartTimerForPath(string path, UploadDestination u)
        {
            lock (Timeouts)
            {
                if (Timeouts.ContainsKey(path))
                {
                    Logger.DebugEntry("Restarting timer for " + path);

                    // Re-start the timer -- the file changed!
                    Timeouts[path].Stop();
                    Timeouts[path].Start();
                }
                else
                {
                    Logger.DebugEntry("Cannot restart timer for " + path + ": not found (starting new one)");

                    // Usually, this is caused by a change without a create.
                    // Strange, but try to handle it like a create anyway.
                    StartTimerForPath(path, u);
                }
            }

            return;
        }

        private void StartTimerForPath(string path, UploadDestination u)
        {
            lock(Timeouts)
            {
                if (!Timeouts.ContainsKey(path))
                {
                    Logger.DebugEntry("Starting timer for " + path);

                    var t = new System.Timers.Timer();

                    t.AutoReset = false;
                    t.Interval = 10000;
                    t.Elapsed += delegate (object s, ElapsedEventArgs args)
                    {
                        Logger.DebugEntry("Timer elapsed for " + path);

                        RemoveTimerForPath(path);
                        UploadQueue.Add(
                            new UploadableFile
                            {
                                Filename = path,
                                UploadDestination = u
                            });

                        return;
                    };

                    Timeouts.Add(path, t);
                    t.Start();
                    return;
                }
            }
            
            Logger.DebugEntry("Timeout list already contains " + path);
        }

        private void RemoveTimerForPath(string path)
        {
            lock(Timeouts)
            {
                if (Timeouts.ContainsKey(path))
                {
                    Logger.DebugEntry("Removing timer for " + path);

                    Timeouts[path].Stop();
                    Timeouts[path].Close();                    
                    Timeouts.Remove(path);
                }
                else
                {
                    Logger.DebugEntry("Cannot remove timer for " + path + ": not found.");
                }
            }
        }


    }
}
