﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Collections.Specialized;

namespace ZaaksysteemUploadService.Properties
{
    public interface ISettings
    {
        bool UpgradeRequired { get; set; }

        StringCollection Name { get; set; }
        StringCollection APIKey { get; set; }
        StringCollection PostURL { get; set; }
        StringCollection WatchPath { get; set; }

        uint HTTPTimeoutMinutes { get; set; }

        void Save();
        void Upgrade();
    }       
}