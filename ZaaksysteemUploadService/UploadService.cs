﻿#region Software License
/*
    Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using System.ServiceModel;
using ZaaksysteemUpload;
using System.Collections.Concurrent;
using System.Reflection;
using System.ServiceModel.Description;

namespace ZaaksysteemUploadService
{
    public partial class UploadService : ServiceBase
    {
        BlockingCollection<UploadableFile> UploadQueue = new BlockingCollection<UploadableFile>();
        CancellationTokenSource _CancellationTokenSource;
        ServiceHost ConfigServiceHost;
        Thread WatcherThread, UploadThread;
        string Version { get; set; }
        ServiceConfiguration ServiceConfiguration;
        public ILogger Logger;

        public UploadService()
        {
            Version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Run();
        }

        protected override void OnStop()
        {
            StopThreads();
            if (ConfigServiceHost != null) { ConfigServiceHost.Close(); }
        }

        protected void OnConfigChange()
        {
            Logger.DebugEntry("Configuration changed. Restarting watcher and upload threads.");

            StopThreads();
            StartThreads();
        }

        internal void Run()
        {
            if (Logger == null)
                Logger = new Logger() { EventLog = EventLog };

            ServiceConfiguration = new ServiceConfiguration(Properties.Settings.Default);

            Logger.WriteEntry("Zaaksysteem Upload Service version " + Version + " starting");

            StartConfigurationService();
            StartThreads();

            return;
        }

        private void StartConfigurationService()
        {
            FrontendCommunicationService cs = new FrontendCommunicationService {
                OnConfigurationChange = OnConfigChange,
                ServiceConfiguration = ServiceConfiguration
            };

            ConfigServiceHost = new ServiceHost(cs, new Uri("net.pipe://localhost"));

#if DEBUG
            // If the service throws an exception while communicating with the client,
            // send the error back for easier debugging.
            ConfigServiceHost.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
            ConfigServiceHost.Description.Behaviors.Add(
                new ServiceDebugBehavior { IncludeExceptionDetailInFaults = true }
            );
#endif

            ConfigServiceHost.AddServiceEndpoint(
                typeof(IConfigurationService),
                new NetNamedPipeBinding(),
                "ZaaksysteemUploadConfiguration"
            );
            ConfigServiceHost.Open();
        }
       
        protected void StartWatcherThread()
        {
            var t = new WatcherThread
            {
                CancellationToken = _CancellationTokenSource.Token,
                Logger = Logger,
                UploadQueue = UploadQueue,
                ServiceConfiguration = ServiceConfiguration,
                Version = Version
            };
            WatcherThread = new Thread(t.Run);
            WatcherThread.Name = "Zaaksysteem Upload Service Directory Watcher";
            WatcherThread.IsBackground = true;
            WatcherThread.Start();
        }

        protected void StartUploadThread()
        {
            var t = new UploadThread {
                CancellationToken = _CancellationTokenSource.Token,
                Logger = Logger,
                UploadQueue = UploadQueue,
                ServiceConfiguration = ServiceConfiguration,
                Version = Version
            };
            UploadThread = new Thread(t.Run);
            UploadThread.Name = "Zaaksysteem Upload Service File Upload Thread";
            UploadThread.IsBackground = true;
            UploadThread.Start();
        }

        void StartThreads()
        {
            _CancellationTokenSource = new CancellationTokenSource();

            StartUploadThread();
            StartWatcherThread();
        }

        void StopThreads()
        {
            _CancellationTokenSource.Cancel();

            Logger.DebugEntry("Stopping watcher thread");
            if (!WatcherThread.Join(2000))
            {
                WatcherThread.Abort();
            }

            Logger.DebugEntry("Stopping upload thread");
            if (!UploadThread.Join(2000))
            {
                UploadThread.Abort();
            }

            _CancellationTokenSource.Dispose();
        }
    }
}
