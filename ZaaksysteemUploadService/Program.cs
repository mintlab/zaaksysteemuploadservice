﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;

namespace ZaaksysteemUploadService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (Environment.UserInteractive)
            {
                Debug.Listeners.Add(new ConsoleTraceListener());

                var s = new UploadService();
                s.Run();

                while (true)
                {
                    Thread.Sleep(1000);
                }
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new UploadService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
