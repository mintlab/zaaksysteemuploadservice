﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Diagnostics;
using System.ServiceModel;
using System.Windows.Forms;
using ZaaksysteemUpload;

namespace ZaaksysteemUploadConfigurator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if DEBUG
            Debug.Listeners.Add(new EventLogTraceListener());
#endif

            // Check if running alone
            // Check if service is active and can be contacted
            ChannelFactory<IConfigurationService> pipeFactory = new ChannelFactory<IConfigurationService>(
                new NetNamedPipeBinding(),
                new EndpointAddress("net.pipe://localhost/ZaaksysteemUploadConfiguration")
            );
            IConfigurationService connection = pipeFactory.CreateChannel();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ConfigurationListForm { ServiceConnection = connection });
        }
    }
}
