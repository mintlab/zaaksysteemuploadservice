﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using ZaaksysteemUpload;

namespace ZaaksysteemUploadConfigurator
{
    public partial class ConfigurationForm : Form
    {
        public ConfigurationForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            configurationNameEntry.Text = ConfigDataItem.Name;
            zaaksysteemUrlEntry.Text = ConfigDataItem.PostURL;
            apiKeyEntry.Text = ConfigDataItem.APIKey;
            selectedFolder.Text = ConfigDataItem.WatchPath;

            // Make sure the form is "save-able" only if it's valid.
            toggleSaveButton();
        }

        private void folderPickButton_Click(object sender, EventArgs e)
        {
            // Open folderpicker, assign result to label
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult dr = fbd.ShowDialog();

            if (dr == DialogResult.OK)
                selectedFolder.Text = fbd.SelectedPath;
        }

        private void configurationNameEntry_TextChanged(object sender, EventArgs e)
        {
            // check if text is filled and not a duplicate (and not original name)
            var matching = ConfigList.Where(ci => {
                // Saving is fine when the configuration name is unchanged, so don't match "ourselves".
                if (ci.Name == ConfigDataItem.Name)
                    return false;

                return ci.Name == configurationNameEntry.Text;
            });

            if (string.IsNullOrWhiteSpace(configurationNameEntry.Text) || matching.Count() != 0)
                configurationNameValid = false;
            else
                configurationNameValid = true;

            toggleSaveButton();
        }

        private void selectedFolder_TextChanged(object sender, EventArgs e)
        {
            // check if text is filled and not a duplicate (and not original name)
            var matching = ConfigList.Where(ci => {
                // Saving is fine when the configuration name is unchanged, so don't match "ourselves".
                if (ci.WatchPath == ConfigDataItem.WatchPath)
                    return false;

                    return ci.WatchPath == selectedFolder.Text;
                });

            if (string.IsNullOrWhiteSpace(selectedFolder.Text) || matching.Count() != 0)
                selectedFolderValid = false;
            else
                selectedFolderValid = true;

            toggleSaveButton();
        }

        private void toggleSaveButton()
        {
            if (configurationNameValid && selectedFolderValid)
                saveButton.Enabled = true;
            else
                saveButton.Enabled = false;

            return;
        }

        public ConfigurationDataItem ConfigDataItem;
        public List<ConfigurationDataItem> ConfigList;

        bool configurationNameValid = false;
        bool selectedFolderValid = false;
    }
}
