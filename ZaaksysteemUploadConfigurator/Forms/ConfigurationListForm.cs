﻿#region Software License
/*
    Copyright (c) 2016, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Windows.Forms;
using ZaaksysteemUpload;

namespace ZaaksysteemUploadConfigurator
{
    public partial class ConfigurationListForm : Form
    {
        public IConfigurationService ServiceConnection;
        ConfigurationData ConfigData;
        BindingSource ConfigItemSource;

        public ConfigurationListForm()
        {
            InitializeComponent();
        }

        private void ConfigurationListForm_Load(object sender, EventArgs e)
        {
            try
            {
                ConfigData = ServiceConnection.GetConfiguration();
            }
            catch (EndpointNotFoundException)
            {
                MessageBox.Show(
                    "Could not contact Zaaksysteem Upload Service. Is it running?",
                    "Error contacting service",
                    MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1
                );

                Application.Exit();
            }
            catch (SecurityAccessDeniedException)
            {
                MessageBox.Show(
                    "Access to Zaaksysteem Upload Service denied.\n\nMake sure you're in the 'Administrators' group.",
                    "Access Denied",
                    MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1
                );

                Application.Exit();
            }

            timeoutMinutes.Value = ConfigData.HTTPTimeoutMinutes;
            ConfigItemSource = new BindingSource { DataSource = ConfigData.ConfigurationItems };
            configListBox.DataSource = ConfigItemSource;
            configListBox.DisplayMember = "Name";
            configListBox.ValueMember = "Name";

            // Make sure "Edit" and "Remove" buttons are disabled if no configuration is selected.
            toggleButtonStates();

            // Only add this *after* setting the initial value. Otherwise, 
            // just starting the configuration program triggers a save/restart
            // on the server side.
            this.timeoutMinutes.ValueChanged += new System.EventHandler(this.timeoutMinutes_ValueChanged);
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            if (configListBox.SelectedIndex != -1)
            {
                // Remove configuration configListBox.SelectedIndex from the stored configuration
                ConfigItemSource.RemoveAt(configListBox.SelectedIndex);
                ServiceConnection.SetConfiguration(ConfigData);
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            // Show a form to create a new configuration
            var cdi = new ConfigurationDataItem();

            if (ShowConfigurationForm(cdi)) {
                ConfigItemSource.Add(cdi);
                ServiceConnection.SetConfiguration(ConfigData);
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            var cdi = (ConfigurationDataItem)configListBox.SelectedItem;
            ShowConfigurationForm(cdi);

            ServiceConnection.SetConfiguration(ConfigData);
        }

        private void configListBox_DoubleClick(object sender, EventArgs e)
        {
            editButton.PerformClick();
        }

        private void configListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            toggleButtonStates();
        }

        private void toggleButtonStates()
        {
            if (configListBox.SelectedIndex == -1)
            {
                deleteButton.Enabled = false;
                editButton.Enabled = false;
            }
            else
            {
                deleteButton.Enabled = true;
                editButton.Enabled = true;
            }
        }

        private bool ShowConfigurationForm(ConfigurationDataItem cdi)
        {
            var cf = new ConfigurationForm {
                ConfigDataItem = cdi,
                ConfigList = ConfigData.ConfigurationItems
            };

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            if (cf.ShowDialog(this) == DialogResult.OK)
            {
                // Gather the new configuration bits from the UI.
                cdi.Name = cf.configurationNameEntry.Text;
                cdi.APIKey = cf.apiKeyEntry.Text;
                cdi.WatchPath = cf.selectedFolder.Text;
                cdi.PostURL = cf.zaaksysteemUrlEntry.Text;
            }
            else
            {
                // Form was cancelled
                return false;
            }
            cf.Dispose();

            return true;
        }

        private void timeoutMinutes_ValueChanged(object sender, EventArgs e)
        {
            ConfigData.HTTPTimeoutMinutes = Convert.ToUInt32(timeoutMinutes.Value);
            ServiceConnection.SetConfiguration(ConfigData);
        }

        private void aboutButton_Click(object sender, EventArgs e)
        {
            using(var about = new AboutBox())
            {
                about.ShowDialog();
            }
        }
    }
}
