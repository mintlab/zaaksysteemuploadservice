﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using ZaaksysteemUploadTests.Utilities;

namespace ZaaksysteemUploadTests.Mocks
{
    class MockSettings : ZaaksysteemUploadService.Properties.ISettings
    {
        public bool UpgradeRequired { get; set; }
        public StringCollection Name { get; set; }
        public StringCollection APIKey { get; set; }
        public StringCollection PostURL { get; set; }
        public StringCollection WatchPath { get; set; }
        public uint HTTPTimeoutMinutes { get; set; }

        public uint SaveCalled = 0;
        public uint UpgradeCalled = 0;

        public bool saved_UpgradeRequired { get; set; }
        public List<string> saved_Name { get; set; }
        public List<string> saved_APIKey { get; set; }
        public List<string> saved_PostURL { get; set; }
        public List<string> saved_WatchPath { get; set; }
        public uint saved_HTTPTimeoutMinutes { get; set; }

        public void Save()
        {
            saved_UpgradeRequired = UpgradeRequired;
            saved_Name = Name.Cast<string>().ToList();
            saved_APIKey = APIKey.Cast<string>().ToList();
            saved_PostURL = PostURL.Cast<string>().ToList();
            saved_WatchPath = WatchPath.Cast<string>().ToList();
            saved_HTTPTimeoutMinutes = HTTPTimeoutMinutes;

            SaveCalled++;
            return;
        }

        public void Upgrade()
        {
            UpgradeCalled++;
        }

        public void AssertState(
            List<string> saved_Name,
            StringCollection Name,
            List<string> saved_APIKey,
            StringCollection APIKey,
            List<string> saved_PostURL,
            StringCollection PostURL,
            List<string> saved_WatchPath,
            StringCollection WatchPath,
            uint saved_HTTPTimeoutMinutes,
            uint HTTPTimeoutMinutes,
            bool saved_UpgradeRequired,
            bool UpgradeRequired,
            uint UpgradeCalled,
            uint SaveCalled)
        {
            if (this.saved_HTTPTimeoutMinutes != saved_HTTPTimeoutMinutes)
                Assert.Fail("Expected HTTPTimeoutMinutes doesn't match saved value");
            if (this.saved_UpgradeRequired != saved_UpgradeRequired)
                Assert.Fail("Expected UpgradeRequired flag doesn't match saved value");
            if (this.UpgradeCalled != UpgradeCalled)
                Assert.Fail("Expected UpgradeCalled count doesn't match");
            if (this.SaveCalled != SaveCalled)
                Assert.Fail("Expected SaveCalled count doesn't match");

            if (!ComparisonUtils.StringCollectionEquals(this.Name, Name))
                Assert.Fail("Names don't match");
            if (!ComparisonUtils.StringCollectionEquals(this.APIKey, APIKey))
                Assert.Fail("API keys don't match");
            if (!ComparisonUtils.StringCollectionEquals(this.PostURL, PostURL))
                Assert.Fail("POST URLs don't match");
            if (!ComparisonUtils.StringCollectionEquals(this.WatchPath, WatchPath))
                Assert.Fail("Watch paths don't match");

            if (!ComparisonUtils.StringListEquals(this.saved_Name, saved_Name))
                Assert.Fail("Saved Names don't match");
            if (!ComparisonUtils.StringListEquals(this.saved_APIKey, saved_APIKey))
                Assert.Fail("Saved API keys don't match");
            if (!ComparisonUtils.StringListEquals(this.saved_PostURL, saved_PostURL))
                Assert.Fail("Saved POST URLs don't match");
            if (!ComparisonUtils.StringListEquals(this.saved_WatchPath, saved_WatchPath))
                Assert.Fail("Saved watch paths don't match");

            return;
        }
    }
}
