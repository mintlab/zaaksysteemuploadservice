﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ZaaksysteemUploadService;
using ZaaksysteemUploadTests.Mocks;

namespace ZaaksysteemUploadTests.Service
{
    [TestClass]
    public class UploadThreadTest
    {
        string CreateAndFillTempFile()
        {
            var filename = Path.GetTempFileName();

            using (var sw = new StreamWriter(filename))
            {
                sw.WriteLine("Test file content");
            }
            return filename;   
        }

        [TestMethod]
        public async Task Test_UploadFile_OK()
        {
            var handler = new MockHttpMessageHandler();
            handler.Response.StatusCode = System.Net.HttpStatusCode.OK;
            handler.Response.ReasonPhrase = "OK";

            var filename = CreateAndFillTempFile();
            var httpClient = new HttpClient(handler);
            var uploadable = new UploadableFile()
            {
                Filename = filename,
                UploadDestination = new UploadDestination()
                {
                    Name = "FakeName",
                    PostURL = "https://not.really/",
                    APIKey = "MyAPIKey"
                }
            };

            var ut = new UploadThread()
            {
                Logger = new MockLogger()
            };

            await ut.UploadFile(httpClient, uploadable);
            
            var body = new StringContent(handler.RequestBody);
            body.Headers.ContentType = handler.RequestContentType;
            var mp = await body.ReadAsMultipartAsync();
            
            var httpParts = new Dictionary<string, string>();
            foreach (var part in mp.Contents) {
                httpParts.Add(part.Headers.ContentDisposition.Name, await part.ReadAsStringAsync());
            }
            var expectedHttpParts = new Dictionary<string, string>();
            expectedHttpParts.Add("api_key", "MyAPIKey");
            expectedHttpParts.Add("filename", "Test file content\r\n");

            Assert.IsFalse(File.Exists(filename));
            Assert.AreEqual(handler.RequestMethod, HttpMethod.Post, "Expected a POST request");
            CollectionAssert.AreEquivalent(
                expectedHttpParts,
                httpParts
            );
        }

        [TestMethod]
        public async Task Test_UploadFile_Failure()
        {
            var handler = new MockHttpMessageHandler();
            handler.Response.StatusCode = System.Net.HttpStatusCode.NotFound;
            handler.Response.ReasonPhrase = "Failed";

            var filename = CreateAndFillTempFile();
            var httpClient = new HttpClient(handler);
            var uploadable = new UploadableFile()
            {
                Filename = filename,
                UploadDestination = new UploadDestination()
                {
                    Name = "FakeName",
                    PostURL = "https://not.really/",
                    APIKey = "MyAPIKey"
                }
            };

            var Logger = new MockLogger();
            var ut = new UploadThread()
            {
                Logger = Logger
            };

            await ut.UploadFile(httpClient, uploadable);

            Assert.IsTrue(File.Exists(filename));
            File.Delete(filename);

            var found = Logger.LoggedMessages.FindAll( x => x.message.Contains("HTTP request failed") );
            Assert.IsTrue(found.Count == 1, "Expected one 'HTTP request failed' log message");
        }

    }
}