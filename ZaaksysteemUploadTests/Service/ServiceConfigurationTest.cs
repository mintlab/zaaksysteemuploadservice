﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZaaksysteemUploadTests.Mocks;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using ZaaksysteemUpload;

namespace ZaaksysteemUploadTests
{
    [TestClass]
    public class ServiceConfigurationTests
    {
        [TestMethod]
        public void Test_Constructor_Empty()
        {
            var ms = new MockSettings()
            {
                Name = new StringCollection(),
                APIKey = new StringCollection(),
                PostURL = new StringCollection(),
                WatchPath = new StringCollection(),
                HTTPTimeoutMinutes = 42,
                UpgradeRequired = true
            };

            var sc = new ZaaksysteemUploadService.ServiceConfiguration(ms);

            ms.AssertState(
                SaveCalled: 1,
                UpgradeCalled: 1,
                Name: new StringCollection(),
                APIKey: new StringCollection(),
                PostURL: new StringCollection(),
                WatchPath: new StringCollection(),
                HTTPTimeoutMinutes: 42,
                UpgradeRequired: false,
                saved_Name: new List<string>(),
                saved_APIKey: new List<string>(),
                saved_PostURL: new List<string>(),
                saved_WatchPath: new List<string>(),
                saved_HTTPTimeoutMinutes: 42,
                saved_UpgradeRequired: false // This is the important bit: constructor marks this false when upgrading
            );
        }

        [TestMethod]
        public void Test_Constructor_NoUpgrade()
        {
            var ms = new MockSettings()
            {
                Name = new StringCollection(),
                APIKey = new StringCollection(),
                PostURL = new StringCollection(),
                WatchPath = new StringCollection(),
                HTTPTimeoutMinutes = 42,
                UpgradeRequired = false
            };

            var sc = new ZaaksysteemUploadService.ServiceConfiguration(ms);

            ms.AssertState(
                SaveCalled: 0,
                UpgradeCalled: 0,
                Name: new StringCollection(),
                APIKey: new StringCollection(),
                PostURL: new StringCollection(),
                WatchPath: new StringCollection(),
                HTTPTimeoutMinutes: 42,
                UpgradeRequired: false,
                saved_Name: null,
                saved_APIKey: null,
                saved_PostURL: null,
                saved_WatchPath: null,
                saved_HTTPTimeoutMinutes: 0, // the default
                saved_UpgradeRequired: false
            );
        }

        [TestMethod]
        public void Test_Constructor_OneItem()
        {
            var n = new StringCollection();
            n.Add("foo");
            var a = new StringCollection();
            a.Add("bar");
            var p = new StringCollection();
            p.Add("https://baz/");
            var w = new StringCollection();
            w.Add("quux");

            var ms = new MockSettings()
            {
                Name = n,
                APIKey = a,
                PostURL = p,
                WatchPath = w,
                HTTPTimeoutMinutes = 42,
                UpgradeRequired = true
            };

            var sc = new ZaaksysteemUploadService.ServiceConfiguration(ms);

            ms.AssertState(
                SaveCalled: 1,
                UpgradeCalled: 1,
                Name: n,
                APIKey: a,
                PostURL: p,
                WatchPath: w,
                HTTPTimeoutMinutes: 42,
                UpgradeRequired: false,
                saved_Name: n.Cast<string>().ToList(),
                saved_APIKey: a.Cast<string>().ToList(),
                saved_PostURL: p.Cast<string>().ToList(),
                saved_WatchPath: w.Cast<string>().ToList(),
                saved_HTTPTimeoutMinutes: 42,
                saved_UpgradeRequired: false
            );
        }

        [TestMethod]
        public void Test_GetConfigurationItems_OneItem()
        {
            var n = new StringCollection();
            n.Add("foo");
            var a = new StringCollection();
            a.Add("bar");
            var p = new StringCollection();
            p.Add("https://baz/");
            var w = new StringCollection();
            w.Add("quux");

            var ms = new MockSettings()
            {
                Name = n,
                APIKey = a,
                PostURL = p,
                WatchPath = w,
                HTTPTimeoutMinutes = 42,
                UpgradeRequired = false
            };

            var sc = new ZaaksysteemUploadService.ServiceConfiguration(ms);

            List<ConfigurationDataItem> items = sc.GetConfigurationItems();
            List<ConfigurationDataItem> expected = new List<ConfigurationDataItem>();
            expected.Add(
                new ConfigurationDataItem()
                {
                    Name = "foo",
                    APIKey = "bar",
                    PostURL = "https://baz/",
                    WatchPath = "quux"
                }
            );
            CollectionAssert.AreEqual(expected, items);
            Assert.AreEqual((uint)42, sc.HTTPTimeoutMinutes);
        }

        [TestMethod]
        public void Test_SetConfiguration_TwoItems()
        {
            var ms = new MockSettings()
            {
                Name = new StringCollection(),
                APIKey = new StringCollection(),
                PostURL = new StringCollection(),
                WatchPath = new StringCollection(),
                HTTPTimeoutMinutes = 42,
                UpgradeRequired = false
            };

            var sc = new ZaaksysteemUploadService.ServiceConfiguration(ms);

            List<ConfigurationDataItem> expected = new List<ConfigurationDataItem>();
            expected.Add(
                new ConfigurationDataItem()
                {
                    Name = "foo",
                    APIKey = "bar",
                    PostURL = "https://baz/",
                    WatchPath = "quux"
                }
            );
            expected.Add(
                new ConfigurationDataItem()
                {
                    Name = "foo2",
                    APIKey = "bar2",
                    PostURL = "https://baz2/",
                    WatchPath = "quux2"
                }
            );
            ConfigurationData cd = new ConfigurationData()
            {
                ConfigurationItems = expected,
                HTTPTimeoutMinutes = 31337
            };
            sc.SetConfiguration(cd);

            var n = new StringCollection();
            n.AddRange(new string[] { "foo", "foo2" });
            var a = new StringCollection();
            a.AddRange(new string[] { "bar", "bar2" });
            var p = new StringCollection();
            p.AddRange(new string[] { "https://baz/", "https://baz2/" });
            var w = new StringCollection();
            w.AddRange(new string[] { "quux", "quux2" });

            ms.AssertState(
                SaveCalled: 1,
                UpgradeCalled: 0,
                Name: n,
                APIKey: a,
                PostURL: p,
                WatchPath: w,
                HTTPTimeoutMinutes: 31337,
                UpgradeRequired: false,
                saved_Name: n.Cast<string>().ToList(),
                saved_APIKey: a.Cast<string>().ToList(),
                saved_PostURL: p.Cast<string>().ToList(),
                saved_WatchPath: w.Cast<string>().ToList(),
                saved_HTTPTimeoutMinutes: 31337,
                saved_UpgradeRequired: false
            );
        }
    }
}